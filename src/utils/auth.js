export function setToken(token) {
  return localStorage.setItem("api_token", token);
}

export function setDataLogin(data) {
  return localStorage.setItem("data_login", data);
}

export function getToken(key) {
  return localStorage.getItem(key);
}

export function removeToken(key) {
  return localStorage.removeItem(key);
}
