import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import Mentoring from "../views/Mentor/Mentoring";
import EditMentoring from "../views/Mentor/EditMentoring";
import LearningMethod from "../views/Mentor/LearningMethod";
import CreateLearningMethod from "../views/Mentor/CreateLearningMethod";
import ProfileMentor from "../views/Mentor/Profile";
import ShowMentor from "../views/Participant/ShowMentor";
import LearningMethodP from "../views/Participant/LearningMethod";
import ChooseMentor from "../views/Participant/ChooseMentor";
import ProfileParticipant from "../views/Participant/Profile";
import { getToken } from '../utils/auth';

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
  if (!getToken("api_token")) {
    next();
    return;
  }

  next("/");
};

const ifAuthenticated = (to, from, next) => {
  if (getToken("api_token")) {
    next();
    return;
  }

  next("/login");
};

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    beforeEnter: ifNotAuthenticated
  },
  {
    path: "/register",
    name: "Register",
    component: Register
  },
  {
    path: "/mentoring",
    name: "Mentoring",
    component: Mentoring,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/mentoring/edit/:participant_id",
    name: "EditMentoring",
    component: EditMentoring,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/learning-method",
    name: "LearningMethod",
    component: LearningMethod,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/learning-method/create",
    name: "CreateLearningMethod",
    component: CreateLearningMethod,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/profile-mentor",
    name: "ProfileMentor",
    component: ProfileMentor,
    beforeEnter: ifAuthenticated
  },


  {
    path: "/show-mentor",
    name: "ShowMentor",
    component: ShowMentor,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/participant/learning-method",
    name: "LearningMethodP",
    component: LearningMethodP,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/participant/choose-mentor",
    name: "ChooseMentor",
    component: ChooseMentor,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/profile-participant",
    name: "ProfileParticipant",
    component: ProfileParticipant,
    beforeEnter: ifAuthenticated
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
